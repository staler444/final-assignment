import 'leaflet/dist/leaflet.css';
import { styled, ThemeProvider } from 'styled-components';
import { connect } from 'react-redux';
import WeatherMap from '/src/components/WeatherMap';
import LoadingIndicator from '/src/components/LoadingIndicator';
import { getTheme } from '/src/themes';
import DarkModeButton from '/src/components/DarkModeButton';
import CenterButton from '/src/components/CenterButton';
import DoubleSlider from '/src/components/DoubleSlider';
import CityNameFilter from '/src/components/CityNameFilter';
import DataRefresher from '/src/components/DataRefresher';

const App = ({ themeName }) => {
  return (
    <ThemeProvider theme={getTheme(themeName)}>
    <Container>
      <WeatherMapContainer>
        <WeatherMap />
      </WeatherMapContainer>
      <OtherContentContainer>
        <DarkModeButton/>
        <DoubleSlider/>
        <CityNameFilter/>
        <LoadingIndicator/>
      </OtherContentContainer>
    <DataRefresher/>
    </Container>
    </ThemeProvider>
  );
};

const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  height: 100vh;
  width: 100vw;
`;
  
const WeatherMapContainer = styled.div`
  flex: 1;
  padding: 5px;
  background-color: lightblue;
`;

const OtherContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: 20px;
  background-color: lightgray;
`;

const mapStateToProps = (state) => ({
  themeName: state.themes.themeName,
});

export default connect(mapStateToProps)(App);
