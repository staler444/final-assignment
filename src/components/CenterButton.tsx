import { connect } from 'react-redux';
import styled from 'styled-components';
import { storeCenterUserLocation } from '/src/store/actions/mapActions'

const StyledButton = styled.button`
  font-size: 16px;
  background-color: ${props => props.theme.background};
  color: ${props => props.theme.text};
  padding: 10px 20px;
  border: none;
  border-radius: 5px;
  cursor: pointer;
  align-self: center;
`;

const CenterButton = ({ dispatch }) => {
  const clickHandler = () => {
    dispatch(storeCenterUserLocation([30.2672, -97.7431]))
  };
  return (
    <StyledButton onClick={clickHandler}>
      Center on user location
    </StyledButton>
  );
};
const mapStateToProps = (state) => ({});

export default connect(mapStateToProps)(CenterButton);
