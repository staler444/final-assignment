import React, { useState } from 'react';
import styled from 'styled-components';
import { setCityNameFilter } from '/src/store/actions/mapActions';
import { connect } from 'react-redux';

const FilterContainer = styled.div`
  display: flex;
  align-items: center;
  background-color: ${(props) => props.theme.background};
  padding: 10px;
  border-radius: 5px;
`;

const FilterInput = styled.input`
  flex: 1;
  padding: 8px;
  border: 1px solid #ccc;
  border-radius: 5px;
`;

const FilterButton = styled.button`
  margin-left: 10px;
  padding: 8px 16px;
  background-color: #333;
  color: white;
  border: none;
  border-radius: 5px;
  cursor: pointer;
`;

const CityNameFilter = ({ dispatch }) => {
  const [cityFilter, setCityFilter] = useState('');

  const handleInputChange = (event) => {
    setCityFilter(event.target.value);
  };

  const handleFilterClick = () => {
    dispatch(setCityNameFilter(cityFilter));
  };

  return (
    <FilterContainer>
      <FilterInput
        type="text"
        placeholder="Enter city name..."
        value={cityFilter}
        onChange={handleInputChange}
      />
      <FilterButton onClick={handleFilterClick}>Filter</FilterButton>
    </FilterContainer>
  );
};

export default connect()(CityNameFilter);
