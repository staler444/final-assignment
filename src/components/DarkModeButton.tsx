import { connect } from 'react-redux';
import styled from 'styled-components';
import { THEME_LIGHT } from '/src/themes/light';
import { THEME_DARK } from '/src/themes/dark';
import { setTheme } from '/src/store/actions/themeActions';

const StyledButton = styled.button`
  font-size: 16px;
  background-color: ${props => props.theme.background};
  color: ${props => props.theme.text};
  padding: 10px 20px;
  border: none;
  border-radius: 5px;
  cursor: pointer;
  align-self: center;
`;

const DarkModeButton = ({ themeName, dispatch }) => {
  const clickHandler = () => {
    dispatch(setTheme(themeName === THEME_LIGHT ? THEME_DARK : THEME_LIGHT))
  };
  return (
    <StyledButton onClick={clickHandler}>
      {themeName === THEME_LIGHT ? "Dark Mode" : "Light Mode"}
    </StyledButton>
  );
};

const mapStateToProps = (state) => ({
  themeName: state.themes.themeName
});

export default connect(mapStateToProps)(DarkModeButton);
