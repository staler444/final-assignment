import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { refreshData } from '/src/store/actions/mapActions';

const DataRefresher = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const intervalId = setInterval(() => {
      dispatch(refreshData());
    }, 3600000);

    return () => clearInterval(intervalId);
  }, []);

  return null;
};

export default DataRefresher;

