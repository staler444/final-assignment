import React, { useState } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { setPopulationRange } from '/src/store/actions/mapActions';

const SliderContainer = styled.div`
  display: flex;
  align-items: center;
`;

const SliderInput = styled.input`
  width: 200px;
  height: 10px;
  margin: 0 5px;
`;

const ValuesContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 200px;
`;

const FilterButton = styled.button`
  margin-left: 10px;
  padding: 8px 16px;
  background-color: #333;
  color: white;
  border: none;
  border-radius: 5px;
  cursor: pointer;
`;

const DoubleSlider = ({ dispatch }) => {
  const min = 0, max = 40000000; // Tokyo
  const [sliderValues, setSliderValues] = useState([min, max]);

  const handleChange = (index, value) => {
    const newValues = [...sliderValues];
    newValues[index] = value;
    setSliderValues(newValues);
  };

  const handleFilter = () => {
    dispatch(setPopulationRange(sliderValues));
  };

  return (
    <SliderContainer>
      <SliderInput
        type="range"
        min={min}
        max={max}
        value={sliderValues[0]}
        onChange={(e) => handleChange(0, parseInt(e.target.value))}
      />
      <SliderInput
        type="range"
        min={min}
        max={max}
        value={sliderValues[1]}
        onChange={(e) => handleChange(1, parseInt(e.target.value))}
      />
      <ValuesContainer>
        <span>{sliderValues[0]}</span>
        <span>{sliderValues[1]}</span>
      </ValuesContainer>
      <FilterButton onClick={handleFilter}>Filter</FilterButton>
    </SliderContainer>
  );
};

export default connect()(DoubleSlider);
