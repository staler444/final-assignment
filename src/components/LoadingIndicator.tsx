import styled from 'styled-components';
import { MagnifyingGlass } from 'react-loader-spinner';
import { connect } from 'react-redux';

const StyledMagnifyingGlass = styled(MagnifyingGlass)`
  height="80"
  width="80"
  ariaLabel="magnifying-glass-loading"
  wrapperStyle={{}}
  wrapperClass="magnifying-glass-wrapper"
  glassColor="#c0efff"
  color="#e15b64"
`;

const LoadingIndicator = ({ isLoading }) => {
  return (
    <StyledMagnifyingGlass visible={isLoading}/>
  )
};

const mapStateToProps = (state) => ({
  isLoading: state.map_.isLoading
});

export default connect(mapStateToProps)(LoadingIndicator);
