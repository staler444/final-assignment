import { useEffect } from 'react';
import { fromEvent } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { useDispatch } from 'react-redux';
import { fetchCitiesRequest, setBounds } from '/src/store/actions/mapActions';
import { useMap } from 'react-leaflet';

const MapEvents = () => {
  const map = useMap();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchCitiesRequest(map.getBounds()));

    const moveend$ = fromEvent(map, 'moveend').pipe(
      debounceTime(1000)
    );

    const subscription = moveend$.subscribe(() => {
      dispatch(fetchCitiesRequest(map.getBounds()));
    });

    return () => {
      subscription.unsubscribe();
    };
  }, []);

  return null;
};

export default MapEvents;
