import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import styled from 'styled-components';
import { connect } from 'react-redux';
import WeatherMarker from './WeatherMarker';
import { THEME_LIGHT } from '/src/themes/light';
import MapEvents from './MapEvents'

const StyledMapContainer = styled(MapContainer)`
  width: 100%;
  height: 100%;
`;

const WeatherMap = ({ weather, mapCenter, themeName, cities, dispatch }) => {
  const lightUrl = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
  const darkUrl = "https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png";

  return (
    <StyledMapContainer center={mapCenter} zoom={13}>
      <TileLayer url={themeName === THEME_LIGHT ? lightUrl : darkUrl} />
      {cities.map(city => (
        <WeatherMarker
          key={city.id}
          city_id={city.id}
          position={[city.lat, city.lon]}
          current={weather[city.id].current}
        />
      ))}
      <MapEvents/>
    </StyledMapContainer>
  );
};

const mapStateToProps = (state) => ({
  cities: state.map_.cities,
  weather: state.map_.weather,
  themeName: state.themes.themeName,
  mapCenter: state.map_.mapCenter
});

export default connect(mapStateToProps)(WeatherMap);
