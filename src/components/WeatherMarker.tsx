import { Marker, Popup } from 'react-leaflet';
import styled from 'styled-components';
import iconsMapping from '/src/utility/iconsMapping';
import { assignNiceness, mapNicenessToEmoji } from '/src/utility/niceness';

const StyledPopup = styled(Popup)`
  color: black;
`;

const WeatherMarker = ({ 
  city_id, 
  position, 
  current // current weather
}) => {
  const icon_code = iconsMapping[current.condition.code];
  const mode = (current.id_day ? "day" : "night");
  const icon_url = `/src/assets/weather/64x64/${mode}/${icon_code}.png`;
  const niceness = assignNiceness(current);
  const emoji = mapNicenessToEmoji[niceness];
  const icon = L.divIcon({
    className: 'custom-icon',
    html: `
      <img src="${icon_url}" alt="Weather Icon" />
      <div style="position: absolute; bottom: 10px; right: -20px; font-size: 20px;">${emoji}</div>
    `,
    iconSize: [32, 32],
    iconAnchor: [32, 64],
  });

  return (
    <Marker key={city_id} position={position} icon={icon}>
      <StyledPopup>
        <div>Temperature: {current.temp_c}°C</div>
        <div>Temperature feels like: {current.feelslike_c}°C</div>
        <div>Condition: {current.condition.text}</div>
        <div>Humidity: {current.humidity}%</div>
        <div>Wind: {current.wind_kph} km/h, {current.wind_dir}</div>
        <div>Precipitation: {current.precip_mm} mm</div>
        <div>Today weather is {niceness}.</div>
      </StyledPopup>
    </Marker>
  );
};

export default WeatherMarker;
