export const FETCH_CITIES_REQUEST       = 'FETCH_CITIES_REQUEST';
export const STORE_CITIES               = 'STORE_CITIES';
export const FETCH_CITIES_FAILURE       = 'FETCH_CITIES_FAILURE';
export const FETCH_WEATHER_REQUEST      = 'FETCH_WEATHER_REQUEST';
export const FETCH_WEATHER_FAILURE      = 'FETCH_WEATHER_FAILURE';
export const STORE_CITIES_AND_WEATHER   = 'STORE_CITIES_AND_WEATHER';
export const FETCH_USER_LOCATION        = 'FETCH_USER_LOCATION';
export const STORE_CENTER_USER_LOCATION = 'STORE_CENTER_USER_LOCATION';
export const REFRESH_DATA               = 'REFRESH_DATA';
export const SET_POPULATION_RANGE       = 'SET_POPULATION_RANGE';
export const SET_BOUNDS                 = 'SET_BOUNDS';
export const SET_CITY_NAME_FILTER       = 'SET_CITY_NAME_FILTER';

export const fetchCitiesRequest = (mapBounds) => ({
  type: FETCH_CITIES_REQUEST,
  payload: mapBounds
});

export const storeCities = (cities, bounds) => ({
  type: STORE_CITIES,
  payload: { 
    cities: cities, 
    bounds: bounds 
  }
});

export const fetchCitiesFailure = (error) => ({
  type: FETCH_CITIES_FAILURE,
  payload: error
});

export const fetchWeatherRequest = (cities, bounds, force = false) => ({
  type: FETCH_WEATHER_REQUEST,
  payload: {
    cities: cities,
    bounds: bounds,
    force: force
  }
});

export const fetchWeatherFailure = (error) => ({
  type: FETCH_WEATHER_FAILURE,
  payload: error
});

export const storeCitiesAndWeather = (cities, weather_patch, bounds) => ({
  type: STORE_CITIES_AND_WEATHER,
  payload: {
    cities: cities,
    weather_patch: weather_patch,
    bounds: bounds
  }
});

export const fetchUserLocation = () => ({
  type: FETCH_USER_LOCATION
});

export const storeCenterUserLocation = (location) => ({
  type: STORE_CENTER_USER_LOCATION,
  payload: location
});

export const setPopulationRange = (range) => ({
  type: SET_POPULATION_RANGE,
  payload: range
});

export const setBounds = (bounds) => ({
  type: SET_BOUNDS,
  payload: bounds
});

export const setCityNameFilter = (cityNameFilter) => ({
  type: SET_CITY_NAME_FILTER,
  payload: cityNameFilter
});

export const refreshData = () => ({
  type: REFRESH_DATA
});
