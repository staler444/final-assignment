export const SET_THEME = 'SET_THEME';

export const setTheme = (themeName) => ({
  type: SET_THEME,
  payload: themeName
});
