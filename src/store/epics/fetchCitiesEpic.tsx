import { ofType } from 'redux-observable';
import { switchMap, map, catchError } from 'rxjs/operators';
import { of, from } from 'rxjs';
import axios from 'axios';
import { 
  FETCH_CITIES_REQUEST,
  SET_POPULATION_RANGE,
  SET_CITY_NAME_FILTER,
  fetchWeatherRequest,
  storeCities,
  fetchCitiesFailure
} from '/src/store/actions/mapActions.tsx';

const isPatchNeeded = (cities, weather) => {
  for (const city of cities)
    if (!(city.id in weather))
      return true;
  return false;
};

const filterCitiesByPopulation = (cities, populationRange) => {
  const [minPopulation, maxPopulation] = populationRange;
  
  return cities.filter(city => {
    const population = city.tags.population;
    return population >= minPopulation && population <= maxPopulation;
  });
};

const top20cities = (cities) => {
  // descending by population
  cities.sort((a, b) => b.tags.population - a.tags.population); 
  cities = cities.slice(0, 20);
  return cities;
};

const filterCitiesByName = (cities, filterString) => {
  return cities.filter(city => {
    return city.tags.name.includes(filterString);
  });
};

const getBounds = (action, state) => {
  return (action.type === FETCH_CITIES_REQUEST ? action.payload : state.currentBounds); 
};

const getRange = (action, state) => {
  return (action.type === SET_POPULATION_RANGE ? action.payload : state.populationRange); 
};

const getStringFilter = (action, state) => {
  return (action.type === SET_CITY_NAME_FILTER ? action.payload : state.cityNameFilter); 
};

const fetchCitiesEpic = (action$, state$) => action$.pipe(
  ofType(FETCH_CITIES_REQUEST, SET_POPULATION_RANGE, SET_CITY_NAME_FILTER),
  switchMap(action => {
    const state = state$.value.map_;
    const bounds = getBounds(action, state);
    const { _southWest, _northEast } = bounds;
    const query = `[out:json];
      node
        [place=city]
        ["name:en"]
        [population]
        (${_southWest.lat},${_southWest.lng},${_northEast.lat},${_northEast.lng});
      out;`;

    return from(axios.get(`https://overpass-api.de/api/interpreter?data=${encodeURIComponent(query)}`))
    .pipe(
      map(response => {
        console.log("miasta", response)
        const weather = state.weather;
        const populationRange = getRange(action, state);
        const filterString = getStringFilter(action, state);
        let cities = response.data.elements;
        cities = filterCitiesByPopulation(cities, populationRange);
        cities = filterCitiesByName(cities, filterString);
        cities = top20cities(cities);

        if (isPatchNeeded(cities, weather))
          return fetchWeatherRequest(cities, bounds);
        return storeCities(cities, bounds);
      }),
      catchError(error => {
        return of(fetchCitiesFailure(error));
      })
    );
  }),
);

export default fetchCitiesEpic;
