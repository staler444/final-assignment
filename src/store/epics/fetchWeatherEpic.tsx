import { ofType } from 'redux-observable';
import { switchMap, map, catchError } from 'rxjs/operators';
import { of, from } from 'rxjs';
import axios from 'axios';
import { 
  FETCH_WEATHER_REQUEST,
  storeCitiesAndWeather,
  fetchWeatherFailure 
} from '/src/store/actions/mapActions.tsx';

const fetchCityWeather = async (lat, lon) => {
  const apiKey = '7f9940cc5d3b4ff6b63192343242802';
  const apiUrl = `http://api.weatherapi.com/v1/current.json?q=${lat},${lon}&key=${apiKey}`;

  const response = await axios.get(apiUrl);
  console.log("response", response)
  return response.data; // Return weather data for the cities
};

const fetchWeather = async (cities, old_weather) => {
  const promises = [];

  for (const city of cities) {
    if (!(city.id in old_weather)) {
      const promise = fetchCityWeather(city.lat, city.lon)
        .then(weather => ({ [city.id]: weather }));
      promises.push(promise);
    }
  }

  const weatherArray = await Promise.all(promises);
  const weather_patch = Object.assign({}, ...weatherArray);
  return weather_patch;
};

const getOldWeather = (action, state) => {
  return (action.payload.force ? {} : state.weather);
};

const fetchWeatherEpic = (action$, state$) => action$.pipe(
  ofType(FETCH_WEATHER_REQUEST),
  switchMap(action => {
    const cities = action.payload.cities;
    const state = state$.value.map_;
    const old_weather = getOldWeather(action, state);

    return from(
      fetchWeather(cities, old_weather)
    ).pipe(
      map(weather_patch => storeCitiesAndWeather(cities, weather_patch, action.payload.bounds)),
      catchError(error => of(fetchWeatherFailure(error)))
    );
  })
);

export default fetchWeatherEpic;
