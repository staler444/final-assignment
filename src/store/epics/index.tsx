import { combineEpics } from 'redux-observable';
import fetchCitiesEpic from './fetchCitiesEpic';
import fetchWeatherEpic from './fetchWeatherEpic';
import refreshData from './refreshDataEpic';

const rootEpic = combineEpics(
  fetchCitiesEpic,
  fetchWeatherEpic,
  refreshData
);

export default rootEpic;
