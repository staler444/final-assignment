import { ofType } from 'redux-observable';
import { switchMap, map, catchError } from 'rxjs/operators';
import { of, from } from 'rxjs';
import axios from 'axios';
import { 
  REFRESH_DATA,
  fetchWeatherRequest,
} from '/src/store/actions/mapActions.tsx';

const refreshDataEpic = (action$, state$) => action$.pipe(
  ofType(REFRESH_DATA),
  switchMap(action => {
    const state = state$.value.map_;
    const cities = state.cities;
    const bounds = state.currentBounds;

    return of(fetchWeatherRequest(cities, bounds, true)); // force = true
  })
);

export default refreshDataEpic;
