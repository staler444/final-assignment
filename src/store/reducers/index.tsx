import { combineReducers } from 'redux';
import mapReducer from './mapReducer';
import themesReducer from './themesReducer';

const rootReducer = combineReducers({
    map_: mapReducer,
    themes: themesReducer
});

export default rootReducer;

