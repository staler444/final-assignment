import { 
  FETCH_CITIES_REQUEST,
  STORE_CITIES,
  FETCH_CITIES_FAILURE,
  STORE_CITIES_AND_WEATHER,
  FETCH_WEATHER_FAILURE,
  FETCH_USER_LOCATION,
  STORE_CENTER_USER_LOCATION,
  SET_POPULATION_RANGE,
  SET_BOUNDS,
  SET_CITY_NAME_FILTER,
  REFRESH_DATA
} from '/src/store/actions/mapActions';

const initialState = {
  weather: {},
  cities: [],
  isLoading: false,
  userLocation: undefined,
  currentBounds: undefined,
  mapCenter: [52.2297, 21.0122],
  populationRange: [0, 40000000],
  cityNameFilter: ""
};

const didCitiesChanged = (new_ones, old_ones) => {
  if (new_ones.length != old_ones.length)
    return true
  for (let i = 0; i < new_ones.length; i++)
    if (new_ones[i].id != old_ones[i].id)
      return true
  return false
};

const citiesReducer = (state = initialState, action) => {
  switch (action.type) {
    case REFRESH_DATA:
    case FETCH_CITIES_REQUEST: {
      return {
        ...state,
        isLoading: true
      };
    }
    case STORE_CITIES: {
      const data = action.payload;
      if (!didCitiesChanged(data.cities, state.cities))
        return {
          ...state,
          currentBounds: data.bounds,
          isLoading: false
        };
      return {
        ...state,
        cities: data.cities,
        currentBounds: data.bounds,
        isLoading: false
      };
    }
    case STORE_CITIES_AND_WEATHER: {
      const data = action.payload;
      return {
        ...state,
        weather: {
          ...state.weather,
          ...data.weather_patch
        },
        cities: data.cities,
        isLoading: false,
        currentBounds: data.bounds
      };
    }
    case FETCH_CITIES_FAILURE: 
    case FETCH_WEATHER_FAILURE: {
      console.error(action.payload.message);
      return {
        ...state,
        isLoading: false
      };
    }
    case STORE_CENTER_USER_LOCATION: {
      const location = action.payload;
      return {
        ...state,
        userLocation: location,
        mapCenter: location
      };
    }
    case SET_POPULATION_RANGE: {
      return {
        ...state,
        populationRange: action.payload,
        isLoading: true
      };
    }
    case SET_BOUNDS: {
      return {
        ...state,
        currentBounds: action.payload
      };
    }
    case SET_CITY_NAME_FILTER: {
      return {
        ...state,
        isLoading: true,
        cityNameFilter: action.payload 
      };
    }
    default:
      return state;
  }
};

export default citiesReducer;
