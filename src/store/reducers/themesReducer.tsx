import { SET_THEME } from '/src/store/actions/themeActions';
import { THEME_LIGHT } from '/src/themes/light';
import { THEME_DARK } from '/src/themes/dark';

const initialState = {
  themeName: THEME_LIGHT
};

const themeReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_THEME: {
      const theme = action.payload;
      return {
        ...state,
        themeName: theme
      };
    }
    default:
      return state;
  }
};

export default themeReducer;
