export const THEME_DARK  = 'THEME_DARK';

export const themeDark = {
  background: '#222222',
  text: '#FFFFFF',
};
