import { THEME_LIGHT, themeLight } from './light';
import { THEME_DARK, themeDark } from './dark';

export const getTheme = (themeName) => {
  switch (themeName) {
    case THEME_LIGHT:
      return themeLight;
    case THEME_DARK:
      return themeDark;
    default:
      return themeLight;
  }
};
