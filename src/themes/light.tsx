export const THEME_LIGHT = 'THEME_LIGHT';

export const themeLight = {
  background: '#FFFFFF',
  text: '#000000',
};
