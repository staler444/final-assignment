export const assignNiceness = (current) => {
  let points = 0;
  if (!current.condition.text.includes("rain"))
    points++;
  if (18 <= current.temp_c && current.temp_c <= 25)
    points++;

  switch (points) {
    case 2:
      return "nice";
    case 1:
      return "passable";
    default:
      return "not nice";
  }
};

export const mapNicenessToEmoji = {
  nice: "😎",
  passable: "😐",
  "not nice": "💀"
};
